resource "aws_lb" "load_balancer" {
    name               = "private-vm-load-balancer"
    internal           = false
    load_balancer_type = "network"
    subnets            = ["${aws_subnet.public_subnet.id}"]
}

resource "aws_lb_listener" "load_balancer_listener" {  
    load_balancer_arn = aws_lb.load_balancer.arn
    port = 22
    protocol = "TCP"
    default_action {    
        target_group_arn = aws_lb_target_group.lb_target_group.arn
        type = "forward"  
    }
}

resource "aws_lb_target_group" "lb_target_group" {
    port = 22
    protocol = "TCP"
    vpc_id = aws_vpc.cloudy.id
}

// Actually attach Private VM to LB
resource "aws_lb_target_group_attachment" "private_vm_lb_attachment" {
  target_group_arn = "${aws_lb_target_group.lb_target_group.arn}"
  target_id        = "${aws_instance.tiny_private_vm.id}"
  port             = 22
}
