resource "aws_vpc" "cloudy" {
    cidr_block = "${var.vpc_cidr}"

    tags = {
        Name = "Virtual Public Cloud"
    }
}

// Public subnet
resource "aws_subnet" "public_subnet" {
    vpc_id = "${aws_vpc.cloudy.id}"

    cidr_block = "${var.public_subnet_cidr}"
    availability_zone = "${var.availability_zone}"
    map_public_ip_on_launch = "true"

    tags = {
        Name = "Public Subnet"
    }
}

// Private subnet
resource "aws_subnet" "private_subnet" {
    vpc_id = "${aws_vpc.cloudy.id}"

    cidr_block = "${var.private_subnet_cidr}"
    availability_zone = "${var.availability_zone}"
    map_public_ip_on_launch = "false"

    tags = {
        Name = "Private Subnet"
    }
}