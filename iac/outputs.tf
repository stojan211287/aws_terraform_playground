output "tiny_public_vm_public_ip" {
  value = "${aws_instance.tiny_public_vm.public_ip}"
}

output "tiny_private_vm_private_ip" {
    value = "${aws_instance.tiny_private_vm.private_ip}"
}

output "lb_dns" {
    value = "${aws_lb.load_balancer.dns_name}"
}