aws_access_key = ""
aws_secret_key = ""

public_key_path = "./pubkeys/stojanjo_aws_test.pub"
private_key_path = "~/.ssh/stojanjo_aws_test"

deployment_region = "eu-west-2"
availability_zone = "eu-west-2a"