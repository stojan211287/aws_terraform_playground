variable "aws_access_key" {}
variable "aws_secret_key" {}

variable "public_key_path" {}
variable "private_key_path" {}
variable deployment_region {
    type = string
}

variable availability_zone {
    type = string
}

variable "vpc_cidr" {
    description = "CIDR for the whole VPC"
    default = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
    description = "CIDR for the Public Subnet"
    default = "10.0.0.0/24"
}

variable "private_subnet_cidr" {
    description = "CIDR for the Private Subnet"
    default = "10.0.1.0/24"
}