# From 
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "tiny_public_vm" {
  ami = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  key_name = "access_key"
  subnet_id = "${aws_subnet.public_subnet.id}"
  associate_public_ip_address = true
  vpc_security_group_ids = ["${aws_security_group.ssh_and_ping.id}"]

  // Boostrap NGINX
  provisioner "remote-exec" {
    connection {
      type = "ssh"
      user = "ubuntu"
      private_key = file("${var.private_key_path}")
      host = "${self.public_ip}"
    }

    inline = [
      "sudo apt update && sudo apt install -y nginx"
    ]
  }
}

resource "aws_instance" "tiny_private_vm" {
  ami = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  iam_instance_profile = "${aws_iam_instance_profile.ec2_profile.name}"
  key_name = "access_key"
  subnet_id = "${aws_subnet.private_subnet.id}"
  associate_public_ip_address = true
  vpc_security_group_ids = ["${aws_security_group.ssh_and_ping.id}"]
}
