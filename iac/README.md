# Main folder hosting Terraform code

## Note

You might need to tweak the public/private key config in order to access
the provisioned VMs

* Public keys to be placed in `./pubkeys`
* Corresponding public & private key path are Terraform variables (see `terraform.tfvars`)

## TF Outputs

* "Public" VM IP
* "Private" VM IP
* DNS name of network load balancer
