resource "aws_iam_role" "iam_role" {
    name = "s3_iam_role"

    assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

/* EC2 instance profile */
resource "aws_iam_instance_profile" "ec2_profile" {
  name = "ec2_profile"
  role = "${aws_iam_role.iam_role.name}"
}

/* IAM S3 policy */
resource "aws_iam_role_policy" "s3_policy" {
  name = "s3_iam_policy"
  role = "${aws_iam_role.iam_role.id}"

  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
        {
        "Action": [
            "s3:*"
        ],
        "Effect": "Allow",
        "Resource": "*"
        }
    ]
    })
}