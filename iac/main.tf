terraform {
    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "~> 3.27"
        }
    }

    backend "s3" {
        bucket = "stojanjoterraformstatebucket"
        key    = "state"
        region = "eu-west-2"
    }

    required_version = "= 1.0.11"
}

provider "aws" {
    profile = "default"
    region = "${var.deployment_region}"
}