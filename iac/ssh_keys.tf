resource "aws_key_pair" "deployer" {
  key_name   = "access_key"
  public_key = file("${var.public_key_path}")
}