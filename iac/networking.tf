// Sec groups for both VMs
resource "aws_security_group" "ssh_and_ping" {
    name = "vpc_public"
    description = "Allow incoming SSH connections & PING"

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    
    ingress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    // Terraform removes the default rule - allow any kind of egress
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    vpc_id = "${aws_vpc.cloudy.id}"

    tags = {
        Name = "VM Sec Group - exposing SSH & ICMP"
    }
}

/* Internet gateway + public VM routing table */

// Internet gateway
resource "aws_internet_gateway" "internet_gateway" {
    vpc_id = "${aws_vpc.cloudy.id}"
    tags = {
        Name = "Internet Gateway"
      }
}

// Public VM routing table
resource "aws_route_table" "public_route_table" {
    vpc_id = "${aws_vpc.cloudy.id}"
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.internet_gateway.id}"
    }
    tags = {
        Name = "Routing Table"
    }
}

// Associate routing table with public subnet
resource "aws_route_table_association" "public_subnet_association" {
    subnet_id      = "${aws_subnet.public_subnet.id}"
    route_table_id = "${aws_route_table.public_route_table.id}"
}

/* NAT gateway + private subnet routing to get to the private VM */

// Elastic IP address needed for the NAT gateway
resource "aws_eip" "nat_eip_address" {
  vpc = true
}

resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.nat_eip_address.id
  subnet_id     = aws_subnet.public_subnet.id
  depends_on    = [aws_internet_gateway.internet_gateway]
}

resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.cloudy.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gateway.id
  }
}

resource "aws_route_table_association" "private_subnet_association" {
  subnet_id      = aws_subnet.private_subnet.id
  route_table_id = aws_route_table.private_route_table.id
}