# Aws Terraform Playground

## Structure

* `iac/` - code setting up a basic infrastructure in AWS
  * One public, one private EC2
  * Public load balancer forwarding SSH to private EC2
  * VPC enveloping all this

## Notes

I used the `aws` python client to auth against AWS before I used Terraform.
For reproducibility, I used `pyenv` to use Python 3.6.8 and `poetry` to
install the proper `aws` version.

With [pyenv](https://github.com/pyenv/pyenv) and [poetry](https://python-poetry.org/) installed, to reproduce the environment, you need to run

* `pyenv install 3.8.6`
* `pyenv local 3.8.6`
* `poetry install`
