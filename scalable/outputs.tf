output ecr_url {
    value = "${aws_ecr_repository.nginx_ecr_repo.repository_url}"
}