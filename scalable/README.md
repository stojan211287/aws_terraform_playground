# Scalable deployment of a conternized workflow on AWS

## Prereqs

* Docker installed on local machine
* Terraform CLI installed
* `poetry` installed
* `pyenv` installed