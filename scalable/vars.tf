
variable deployment_region {
    type = string
}

variable number_of_containers {
    type = number
}

variable container_port {
  type = number
}
